FROM golang:1.10-alpine as builder
WORKDIR /go/src/github.com/Hendra-Huang/loginapp
RUN apk add --no-cache make
COPY . /go/src/github.com/Hendra-Huang/loginapp
RUN make build

FROM alpine:3.6 as loginserver
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/github.com/Hendra-Huang/loginapp/bin/ /usr/local/bin/
EXPOSE 5555
ENTRYPOINT ["/usr/local/bin/loginserver"]
