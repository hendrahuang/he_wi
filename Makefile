appenv := $(shell cat ./env_${ENV})

run-loginserver:
	${appenv} go run ./cmd/loginserver/main.go

build:
	CGO_ENABLED=0 go build -o bin/loginserver ./cmd/loginserver/main.go

unit-test:
	${appenv} go test -v ./...

partial-test:
	${appenv} go test -v -tags=integration ${ARGS}

test:
	${appenv} go test -v -tags=integration ./...

.PHONY: run-loginserver build unit-test partial-test test
