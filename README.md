# loginapp

## About the project

This is a small app that handles registration, login, and update profile. I wrap the router and provides basic monitoring using prometheus. I use [dep](https://github.com/golang/dep) for depedency management. There is simple opentracing implementation in this app. This app is provided with parallel database integration test.

Some sample API:
1. Register
```
curl -X POST \
  http://localhost:5555/api/v1/register \
  -H 'Content-Type: application/json' \
  -d '{
	"email": "asd@asd.com",
	"password": "pass"
}'
```
2. Login
```
curl -X POST \
  http://localhost:5555/api/v1/login \
  -H 'Content-Type: application/json' \
  -d '{
	"email": "asd@asd.com",
	"password": "pass"
}'
```
3. Complete profile information
```
curl -X PUT \
  http://localhost:5555/api/v1/me/profile \
  -H 'Content-Type: application/json' \
  -H 'Cookie: sid=ea8a099413147ac4e3eb1106309eb4d82927334c9941cd391ebd67b883144508; path=/api/v1; domain=localhost; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;' \
  -d '{
	"fullname": "Asd",
	"address": "address",
	"telephone": "111111"
}'
```
4. Get user
```
curl -X GET \
  http://localhost:5555/api/v1/me \
  -H 'Content-Type: application/json' \
  -H 'Cookie: sid=ea8a099413147ac4e3eb1106309eb4d82927334c9941cd391ebd67b883144508; path=/api/v1; domain=localhost; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;'
```
5. Update user
```
curl -X PUT \
  http://localhost:5555/api/v1/me \
  -H 'Content-Type: application/json' \
  -H 'Cookie: sid=ea8a099413147ac4e3eb1106309eb4d82927334c9941cd391ebd67b883144508; path=/api/v1; domain=localhost; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;' \
  -d '{
	"email": "asd@asd.com",
	"fullname": "Asd3",
	"address": "address3",
	"telephone": "2131231"
}'
```

## About the structure

The files in the root package contains domain logic (business logic) of the app. `cmd` contains entrypoint of the app / command. `script` contains shell script that can help to automate the build process, etc. Currently, `script` contains script for running test with code coverage preview. The rest of folders are the dependencies of the app. The data layer is at `mysql` package. You can find list of endpoints in `router` package. HTTP handler is located in `handler` package inside `server` folder.

All Go files at the root level are domain entities and business logic. All folders at the root level are the dependencies for the business logic. All dependencies are being created and wired at main.go (inside cmd folder). By using this approach, each package can be tested independently.
