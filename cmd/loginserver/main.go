package main

import (
	"strconv"
	"time"

	"github.com/Hendra-Huang/loginapp"
	"github.com/Hendra-Huang/loginapp/env"
	"github.com/Hendra-Huang/loginapp/errorutil"
	"github.com/Hendra-Huang/loginapp/log"
	"github.com/Hendra-Huang/loginapp/mysql"
	"github.com/Hendra-Huang/loginapp/passwordmanager"
	"github.com/Hendra-Huang/loginapp/redis"
	"github.com/Hendra-Huang/loginapp/router"
	"github.com/Hendra-Huang/loginapp/server"
	"github.com/Hendra-Huang/loginapp/server/handler"
	"github.com/Hendra-Huang/loginapp/session"
	"github.com/Hendra-Huang/loginapp/tracing"
	jaegerprom "github.com/uber/jaeger-lib/metrics/prometheus"
)

func main() {
	// initialize tracer
	tracer := tracing.NewTracer("mayappserver", jaegerprom.New(), "0.0.0.0:6831")

	// initialize database
	db, err := mysql.New(mysql.Options{
		DBHost:     env.GetWithDefault("DB_HOST", "127.0.0.1"),
		DBPort:     env.GetWithDefault("DB_PORT", "3306"),
		DBUser:     "loginapp",
		DBPassword: "loginapp",
		DBName:     "loginapp",
	})
	errorutil.CheckWithErrorHandler(err, func(err error) {
		log.Errors(err)
		log.Fatal("Failed to initialize database")
	})

	// initialize redis
	envRedisDB := env.GetWithDefault("REDIS_DB", "0")
	redisDB, err := strconv.Atoi(envRedisDB)
	if err != nil {
		redisDB = 0
	}
	redisClient := redis.NewClient(env.Get("REDIS_ADDRESS"), env.Get("REDIS_PASSWORD"), redisDB)

	// initialize session
	sessionStore := session.NewStore(env.GetWithDefault("SESSION_SALT", "randomsalt"), redisClient)

	// initialize repository
	userRepository := mysql.NewUserRepository(tracer, db, db)

	// initialize service
	passwordEncryptionService := passwordmanager.NewEncryptionService(env.GetWithDefault("PASSWORD_SALT", "randomsalt"))
	userService := loginapp.NewUserService(tracer, userRepository, passwordEncryptionService, sessionStore)

	// initialize handler
	pingHandler := handler.NewPingHandler()
	userHandler := handler.NewUserHandler(userService)

	server := server.New(server.Options{
		ListenAddress: ":5555",
	})

	r := router.New(router.Options{
		Timeout: 5 * time.Second,
	}, tracer, sessionStore)

	router.RegisterRoute(r, pingHandler, userHandler)
	err = server.Serve(r)
	if err != nil {
		log.Fatalf("Error starting webserver. %s\n", err.Error())
	}
}
