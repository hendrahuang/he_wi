package mock

import (
	"context"
	"errors"

	"github.com/Hendra-Huang/loginapp"
)

type UserRepository struct{}

func (ur *UserRepository) FindAll(ctx context.Context) ([]loginapp.User, error) {
	users := []loginapp.User{
		loginapp.User{
			ID:       1,
			Email:    "test1@example.com",
			Password: "test1",
		},
		loginapp.User{
			ID:       2,
			Password: "test2",
			Email:    "test2@example.com",
		},
	}

	return users, nil
}

func (ur *UserRepository) FindByID(ctx context.Context, id int64) (loginapp.User, error) {
	user := loginapp.User{
		ID:       id,
		Password: "test",
		Email:    "test@example.com",
	}

	return user, nil
}

func (ur *UserRepository) Create(ctx context.Context, email, name string) error {
	return nil
}

type UserRepositoryWithError struct{}

func (ur *UserRepositoryWithError) FindAll(ctx context.Context) ([]loginapp.User, error) {
	return nil, errors.New("internal error")
}

func (ur *UserRepositoryWithError) FindByID(ctx context.Context, id int64) (loginapp.User, error) {
	user := loginapp.User{}

	return user, errors.New("internal error")
}

func (ur *UserRepositoryWithError) Create(ctx context.Context, email, name string) error {
	return errors.New("internal error")
}
