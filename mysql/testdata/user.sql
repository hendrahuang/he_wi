CREATE TABLE IF NOT EXISTS users (
    id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(256) NOT NULL,
    fullname VARCHAR(100),
    address VARCHAR(100),
    telephone VARCHAR(50),
    PRIMARY KEY (`id`)
);

INSERT INTO users(id, email, password) VALUES (DEFAULT, 'myuser@example.com', 'password');
INSERT INTO users(id, email, password) VALUES (DEFAULT, 'myuser2@example.com', 'password2');
