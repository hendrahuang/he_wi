package mysql

import (
	"context"
	"database/sql"

	"github.com/Hendra-Huang/loginapp"
	"github.com/jmoiron/sqlx"
	opentracing "github.com/opentracing/opentracing-go"
)

type (
	userPreparedStatements struct {
		findAll           *sqlx.Stmt
		findByID          *sqlx.Stmt
		findByEmail       *sqlx.Stmt
		findByCredentials *sqlx.Stmt
		create            *sqlx.Stmt
		updateProfile     *sqlx.Stmt
		update            *sqlx.Stmt
	}

	UserRepository struct {
		tracer     opentracing.Tracer
		Master     *DB
		Slave      *DB
		statements userPreparedStatements
	}
)

func NewUserRepository(tracer opentracing.Tracer, master, slave *DB) *UserRepository {
	findAllQuery := `SELECT id, email, fullname, address, telephone FROM users`
	findByIDQuery := `SELECT id, email, fullname, address, telephone FROM users where id = ?`
	findByEmailQuery := `SELECT id, email, fullname, address, telephone FROM users where email = ?`
	findByCredentialsQuery := `SELECT id, email, fullname, address, telephone FROM users where email = ? AND password = ?`
	createQuery := `INSERT INTO users(email, password) VALUES (?, ?)`
	updateProfileQuery := `UPDATE users SET fullname = ?, address = ?, telephone = ? WHERE id = ?`
	updateQuery := `UPDATE users SET email = ?, fullname = ?, address = ?, telephone = ? WHERE id = ?`

	findAllStmt := slave.SafePreparex(findAllQuery)
	findByIDStmt := slave.SafePreparex(findByIDQuery)
	findByEmailStmt := slave.SafePreparex(findByEmailQuery)
	findByCredentialsStmt := slave.SafePreparex(findByCredentialsQuery)
	createStmt := master.SafePreparex(createQuery)
	updateProfileStmt := master.SafePreparex(updateProfileQuery)
	updateStmt := master.SafePreparex(updateQuery)

	preparedStatements := userPreparedStatements{
		findAll:           findAllStmt,
		findByID:          findByIDStmt,
		findByEmail:       findByEmailStmt,
		findByCredentials: findByCredentialsStmt,
		create:            createStmt,
		updateProfile:     updateProfileStmt,
		update:            updateStmt,
	}

	return &UserRepository{
		tracer:     tracer,
		Master:     master,
		Slave:      slave,
		statements: preparedStatements,
	}
}

func (ur *UserRepository) FindAll(ctx context.Context) ([]loginapp.User, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.FindAll", opentracing.ChildOf(span.Context()))
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	var users []loginapp.User
	err := ur.statements.findAll.SelectContext(ctx, &users)
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (ur *UserRepository) FindByID(ctx context.Context, id int64) (loginapp.User, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.FindByID", opentracing.ChildOf(span.Context()))
		span.SetTag("id", id)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	var user loginapp.User
	err := ur.statements.findByID.GetContext(ctx, &user, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, nil
		}
		return user, err
	}

	return user, nil
}

func (ur *UserRepository) FindByEmail(ctx context.Context, email string) (loginapp.User, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.FindByEmail", opentracing.ChildOf(span.Context()))
		span.SetTag("email", email)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	var user loginapp.User
	err := ur.statements.findByEmail.GetContext(ctx, &user, email)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, nil
		}
		return user, err
	}

	return user, nil
}

func (ur *UserRepository) FindByCredentials(ctx context.Context, email, password string) (loginapp.User, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.FindByCredentials", opentracing.ChildOf(span.Context()))
		span.SetTag("email", email)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	var user loginapp.User
	err := ur.statements.findByCredentials.GetContext(ctx, &user, email, password)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, nil
		}
		return user, err
	}

	return user, nil
}

func (ur *UserRepository) Create(ctx context.Context, email, password string) (int64, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.Create", opentracing.ChildOf(span.Context()))
		span.SetTag("email", email)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	result, err := ur.statements.create.ExecContext(ctx, email, password)
	if err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

func (ur *UserRepository) UpdateProfile(ctx context.Context, id int64, fullname, address, telephone string) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.UpdateProfile", opentracing.ChildOf(span.Context()))
		span.SetTag("id", id)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	_, err := ur.statements.updateProfile.ExecContext(ctx, fullname, address, telephone, id)
	if err != nil {
		return err
	}

	return nil
}

func (ur *UserRepository) Update(ctx context.Context, user loginapp.User) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := ur.tracer.StartSpan("UserRepository.Update", opentracing.ChildOf(span.Context()))
		span.SetTag("id", user.ID)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	_, err := ur.statements.update.ExecContext(ctx, user.Email, user.Fullname, user.Address, user.Telephone, user.ID)
	if err != nil {
		return err
	}

	return nil
}
