// +build integration

package mysql_test

import (
	"context"
	"testing"

	"github.com/Hendra-Huang/loginapp/mysql"
	"github.com/Hendra-Huang/loginapp/testingutil"
	"github.com/opentracing/opentracing-go/mocktracer"
)

func TestFindAll(t *testing.T) {
	t.Parallel()
	db, _, cleanup := mysql.CreateTestDatabase(t)
	defer cleanup()

	mysql.LoadFixtures(t, db, "user")

	tracer := mocktracer.New()
	ur := mysql.NewUserRepository(tracer, db, db)
	users, err := ur.FindAll(context.Background())
	testingutil.Ok(t, err)
	testingutil.Equals(t, 2, len(users))
	testingutil.Equals(t, int64(1), users[0].ID)
	testingutil.Equals(t, "myuser@example.com", users[0].Email)
}

func TestFindByID(t *testing.T) {
	t.Parallel()
	db, _, cleanup := mysql.CreateTestDatabase(t)
	defer cleanup()

	mysql.LoadFixtures(t, db, "user")

	tracer := mocktracer.New()
	ur := mysql.NewUserRepository(tracer, db, db)
	ctx := context.Background()

	testCases := []struct {
		userID        int64
		expectedEmail string
		expectedError error
	}{
		{
			userID:        1,
			expectedEmail: "myuser@example.com",
			expectedError: nil,
		},
		{
			userID:        1000,
			expectedEmail: "",
			expectedError: nil,
		},
	}
	for _, tc := range testCases {
		user, err := ur.FindByID(ctx, tc.userID)
		testingutil.Equals(t, tc.expectedError, err)
		testingutil.Equals(t, tc.expectedEmail, user.Email)
	}
}

func TestCreateUser(t *testing.T) {
	t.Parallel()
	db, _, cleanup := mysql.CreateTestDatabase(t)
	defer cleanup()

	mysql.LoadFixtures(t, db, "user")

	tracer := mocktracer.New()
	ur := mysql.NewUserRepository(tracer, db, db)
	ctx := context.Background()

	testCases := []struct {
		email         string
		password      string
		expectedError error
	}{
		{
			email:         "test@example.com",
			password:      "test",
			expectedError: nil,
		},
	}
	for _, tc := range testCases {
		_, err := ur.Create(ctx, tc.email, tc.password)
		testingutil.Equals(t, tc.expectedError, err)
	}
}
