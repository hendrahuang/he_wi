package passwordmanager

import (
	"crypto/sha256"
	"fmt"
)

type EncryptionService struct {
	salt string
}

func NewEncryptionService(salt string) *EncryptionService {
	return &EncryptionService{
		salt: salt,
	}
}

func (es *EncryptionService) Encrypt(input string) string {
	h := sha256.New()
	h.Write([]byte(fmt.Sprintf("%s%s", es.salt, input)))

	return fmt.Sprintf("%x", h.Sum(nil))
}
