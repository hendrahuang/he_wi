package redis

import goredis "github.com/go-redis/redis"

type Client struct {
	client *goredis.Client
}

func NewClient(address, password string, db int) *Client {
	client := goredis.NewClient(&goredis.Options{
		Addr:     address,
		Password: password,
		DB:       db,
	})

	return &Client{
		client: client,
	}
}

func (c *Client) Set(key, value string) error {
	return c.client.Set(key, value, 0).Err()
}

func (c *Client) Get(key string) (string, error) {
	value, err := c.client.Get(key).Result()
	if err != nil {
		if err == goredis.Nil {
			return "", nil
		}
		return "", err
	}

	return value, nil
}
