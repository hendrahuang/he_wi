package router

import (
	"net/http"

	"github.com/Hendra-Huang/loginapp/server/handler"
	"github.com/prometheus/client_golang/prometheus"
)

func RegisterRoute(rtr *Router, pingHandler *handler.PingHandler, userHandler *handler.UserHandler) {
	rtr.Get("/metrics", func(w http.ResponseWriter, r *http.Request) {
		prometheus.Handler()
	})
	rtr.Get("/ping", pingHandler.Ping)

	apiRouter := rtr.SubRouter("/api/v1")
	apiRouter.Post("/register", userHandler.Register)
	apiRouter.Post("/login", userHandler.Login)
	apiRouter.Put("/me/profile", rtr.secure(userHandler.UpdateMyProfile))
	apiRouter.Get("/me", rtr.secure(userHandler.GetMe))
	apiRouter.Put("/me", rtr.secure(userHandler.UpdateMe))
}
