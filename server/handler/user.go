package handler

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/Hendra-Huang/loginapp"
	"github.com/Hendra-Huang/loginapp/log"
	"github.com/Hendra-Huang/loginapp/server/responseutil"
)

type (
	UserServicer interface {
		Register(ctx context.Context, email, password string) (int64, error)
		Login(ctx context.Context, email, password string) (string, error)
		UpdateProfile(ctx context.Context, id int64, fullname, address, telephone string) error
		Update(ctx context.Context, user loginapp.User) error
		FindByID(context.Context, int64) (loginapp.User, error)
	}

	UserHandler struct {
		userService UserServicer
	}

	UserLoginRequest struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	UserRegisterRequest struct {
		UserLoginRequest
	}
	UpdateProfileRequest struct {
		Fullname  string `json:"fullname"`
		Address   string `json:"address"`
		Telephone string `json:"telephone"`
	}
)

func NewUserHandler(us UserServicer) *UserHandler {
	return &UserHandler{us}
}

func (uh *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseutil.BadRequest(w, "Fail reading the request body")
		return
	}
	defer r.Body.Close()
	registerRequest := UserLoginRequest{}
	if err := json.Unmarshal(body, &registerRequest); err != nil {
		responseutil.BadRequest(w, "Fail to parse request body")
		return
	}

	sessionID, err := uh.userService.Login(ctx, registerRequest.Email, registerRequest.Password)
	if err != nil {
		if err == loginapp.ErrLoginFailed {
			responseutil.JSON(w, nil, responseutil.WithStatus(http.StatusForbidden))
			return
		}
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}

	cookie := http.Cookie{
		Name:     "sid",
		Value:    sessionID,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)

	responseutil.JSON(w, nil, responseutil.WithStatus(http.StatusOK))
}

func (uh *UserHandler) Register(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseutil.BadRequest(w, "Fail reading the request body")
		return
	}
	defer r.Body.Close()
	registerRequest := UserRegisterRequest{}
	if err := json.Unmarshal(body, &registerRequest); err != nil {
		responseutil.BadRequest(w, "Fail to parse request body")
		return
	}

	id, err := uh.userService.Register(ctx, registerRequest.Email, registerRequest.Password)
	if err != nil {
		if err == loginapp.ErrEmailExists {
			responseutil.BadRequest(w, "Email already exists")
			return
		}
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}

	responseutil.JSON(w, map[string]interface{}{"id": id}, responseutil.WithStatus(http.StatusOK))
}

func (uh *UserHandler) GetMe(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userSession, err := getUserSessionFromContext(ctx)
	if err != nil {
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}

	user, err := uh.userService.FindByID(ctx, userSession.ID)
	if err != nil {
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}
	if user.ID == 0 {
		responseutil.NotFound(w)
		return
	}

	responseutil.JSON(w, user, responseutil.WithStatus(http.StatusOK))
}

func (uh *UserHandler) UpdateMyProfile(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userSession, err := getUserSessionFromContext(ctx)
	if err != nil {
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseutil.BadRequest(w, "Fail reading the request body")
		return
	}
	defer r.Body.Close()
	updateProfileRequest := UpdateProfileRequest{}
	if err := json.Unmarshal(body, &updateProfileRequest); err != nil {
		responseutil.BadRequest(w, "Fail to parse request body")
		return
	}

	if err := uh.userService.UpdateProfile(ctx, userSession.ID, updateProfileRequest.Fullname, updateProfileRequest.Address, updateProfileRequest.Telephone); err != nil {
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}
}

func (uh *UserHandler) UpdateMe(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userSession, err := getUserSessionFromContext(ctx)
	if err != nil {
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responseutil.BadRequest(w, "Fail reading the request body")
		return
	}
	defer r.Body.Close()
	user := loginapp.User{}
	if err := json.Unmarshal(body, &user); err != nil {
		responseutil.BadRequest(w, "Fail to parse request body")
		return
	}
	user.ID = userSession.ID

	if err := uh.userService.Update(ctx, user); err != nil {
		if err == loginapp.ErrUserNotFound {
			responseutil.NotFound(w)
			return
		}
		log.Errors(err)
		responseutil.InternalServerError(w)
		return
	}
}

func getUserSessionFromContext(ctx context.Context) (loginapp.User, error) {
	userSession, ok := ctx.Value("UserSession").(loginapp.User)
	if !ok {
		return loginapp.User{}, errors.New("Fail to get user from request context")
	}

	return userSession, nil
}
