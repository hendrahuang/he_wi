package session

import (
	"crypto/sha256"
	"fmt"
)

type (
	Store struct {
		salt    string
		storage Storage
	}

	Storage interface {
		Set(string, string) error
		Get(string) (string, error)
	}
)

func NewStore(salt string, storage Storage) *Store {
	return &Store{
		salt:    salt,
		storage: storage,
	}
}

func (s *Store) Set(key, value string) (string, error) {
	encryptedKey := s.encryptSessionKey(key)
	if err := s.storage.Set(encryptedKey, value); err != nil {
		return "", err
	}

	return encryptedKey, nil
}

func (s *Store) Get(key string) (string, error) {
	return s.storage.Get(key)
}

func (s *Store) encryptSessionKey(key string) string {
	h := sha256.New()
	h.Write([]byte(fmt.Sprintf("%s%s", s.salt, key)))

	return fmt.Sprintf("%x", h.Sum(nil))
}
