package loginapp

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	opentracing "github.com/opentracing/opentracing-go"
	"gopkg.in/guregu/null.v3"
)

type (
	User struct {
		ID        int64       `json:"id"`
		Email     string      `json:"email"`
		Password  string      `json:"-"`
		Fullname  null.String `json:"fullname"`
		Address   null.String `json:"address"`
		Telephone null.String `json:"telephone"`
	}

	UserService struct {
		tracer            opentracing.Tracer
		userRepository    UserRepository
		passwordEncryptor PasswordEncryptor
		sessionStorer     SessionStorer
	}

	UserRepository interface {
		Create(context.Context, string, string) (int64, error)
		UpdateProfile(context.Context, int64, string, string, string) error
		FindByID(context.Context, int64) (User, error)
		FindByEmail(context.Context, string) (User, error)
		FindByCredentials(context.Context, string, string) (User, error)
		Update(context.Context, User) error
	}

	PasswordEncryptor interface {
		Encrypt(input string) string
	}

	SessionStorer interface {
		Set(string, string) (string, error)
	}
)

var (
	ErrUserNotFound = errors.New("User not found")
	ErrEmailExists  = errors.New("Email already exists")
	ErrLoginFailed  = errors.New("Login failed")
)

func NewUserService(tracer opentracing.Tracer, ur UserRepository, pe PasswordEncryptor, ss SessionStorer) *UserService {
	return &UserService{
		tracer:            tracer,
		userRepository:    ur,
		passwordEncryptor: pe,
		sessionStorer:     ss,
	}
}

func (us *UserService) Register(ctx context.Context, email, plainPassword string) (int64, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := us.tracer.StartSpan("UserService.Register", opentracing.ChildOf(span.Context()))
		span.SetTag("email", email)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	// check does the email exist
	user, err := us.userRepository.FindByEmail(ctx, email)
	if err != nil {
		return 0, err
	}
	if user.ID != 0 {
		return 0, ErrEmailExists
	}

	encrytpedPassword := us.passwordEncryptor.Encrypt(plainPassword)
	id, err := us.userRepository.Create(ctx, email, encrytpedPassword)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (us *UserService) Login(ctx context.Context, email, plainPassword string) (string, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := us.tracer.StartSpan("UserService.Login", opentracing.ChildOf(span.Context()))
		span.SetTag("email", email)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	encrytpedPassword := us.passwordEncryptor.Encrypt(plainPassword)
	user, err := us.userRepository.FindByCredentials(ctx, email, encrytpedPassword)
	if err != nil {
		return "", err
	}
	if user.ID == 0 {
		return "", ErrLoginFailed
	}

	// TODO create redis session
	sessionKey := fmt.Sprintf("user_session_%d", user.ID)
	sessionValue, err := json.Marshal(user)
	if err != nil {
		return "", err
	}
	encryptedKey, err := us.sessionStorer.Set(sessionKey, string(sessionValue))
	if err != nil {
		return "", err
	}

	return encryptedKey, nil
}

func (us *UserService) UpdateProfile(ctx context.Context, id int64, fullname, address, telephone string) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := us.tracer.StartSpan("UserService.UpdateProfile", opentracing.ChildOf(span.Context()))
		span.SetTag("id", id)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	return us.userRepository.UpdateProfile(ctx, id, fullname, address, telephone)
}

func (us *UserService) FindByID(ctx context.Context, id int64) (User, error) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := us.tracer.StartSpan("UserService.FindByID", opentracing.ChildOf(span.Context()))
		span.SetTag("id", id)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	return us.userRepository.FindByID(ctx, id)
}

func (us *UserService) Update(ctx context.Context, userParams User) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := us.tracer.StartSpan("UserService.Update", opentracing.ChildOf(span.Context()))
		span.SetTag("id", userParams.ID)
		defer span.Finish()
		ctx = opentracing.ContextWithSpan(ctx, span)
	}

	// check does the id exist
	user, err := us.userRepository.FindByID(ctx, userParams.ID)
	if err != nil {
		return err
	}
	if user.ID == 0 {
		return ErrUserNotFound
	}

	if err := us.userRepository.Update(ctx, userParams); err != nil {
		return err
	}

	return nil
}
